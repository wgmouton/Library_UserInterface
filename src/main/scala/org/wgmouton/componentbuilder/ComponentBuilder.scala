package org.wgmouton.componentbuilder

import org.wgmouton._
import org.wgmouton.componentbuilder.Components.Element
import play.twirl.api.{Html, HtmlFormat}

import scala.language.implicitConversions


/**
  * Project: Play_UI_Builder_v2
  * Created by Willem Mouton <wmouton@fullfacing.com> at Full Facing on 2016/02/29.
  */
//object ComponentBuilder {
//  def HelloWorld(): Html = components.html.table.render()
//
//}

object Components extends AnyRef {

  sealed trait Element

  case class Table(s: String) extends Element
}

sealed trait ElementBuilder[+A] {
  def toElement[B <: Element](f: A => B): B
}

sealed trait HtmlBuilder[+A] {
  def toHtml(implicit f: A => Html): Html
}

object ComponentBuilder {
  implicit def elementBuilder[A <: Any](a: A): ElementBuilder[A] = new ElementBuilder[A] {
    override def toElement[B <: Element](f: A => B): B = f(a)
  }

  implicit def htmlBuilder[A <: Element](e: A): HtmlBuilder[A] = new HtmlBuilder[A] {
    override def toHtml(implicit f: A => Html): Html = f(e)
  }

  val helloWorld = ""//hallo".toElement(Components.Table).toHtml(org.wgmouton.components.html.table.render(_))
}