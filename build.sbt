name := "Library_UserInterface"

version := {
  lazy val majorVersion = 0
  lazy val minorVersion = 1
  lazy val microVersion = sys.env.getOrElse("CI_BUILD_ID", "0")
  /*
  * 0 = Production
  * 1 = Development
  */
  sys.props.getOrElse("mode", "1").toInt match {
    case 0 => s"$majorVersion.$minorVersion.0"
    case 1 => s"$majorVersion.$minorVersion.$microVersion-SNAPSHOT"
  }
}

organization := "org.wgmouton"

scalaVersion := "2.11.7"

resolvers ++= Seq(
  "Sonatype OSS Snapshots" at "http://ec2-52-33-25-25.us-west-2.compute.amazonaws.com:8081/nexus/content/repositories/snapshots",
  "Sonatype OSS Releases" at "http://ec2-52-33-25-25.us-west-2.compute.amazonaws.com:8081/nexus/content/repositories/releases"
)

lazy val `library_userInterface` = (project in file(".")).enablePlugins(SbtTwirl)

TwirlKeys.templateImports += "org.wgmouton.components._"

sourceDirectories in(Compile, TwirlKeys.compileTemplates) := (unmanagedSourceDirectories in Compile).value

// Publish to nexus
lazy val nexus_host = sys.env.getOrElse("NEXUS_HOST", "")
lazy val nexus_username = sys.env.getOrElse("NEXUS_USERNAME", "")
lazy val nexus_password = sys.env.getOrElse("NEXUS_PASSWORD", "")

publishTo := {
  val nexus = s"http://$nexus_host:8081/nexus/"
  if (version.value.trim.endsWith("SNAPSHOT"))
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases" at nexus + "content/repositories/releases")
}

credentials += Credentials("Nexus Repository Manager", nexus_host, nexus_username, nexus_password)

publishArtifact in Test := false

libraryDependencies ++= {
  Seq(
    "ch.qos.logback" % "logback-core" % "1.1.3",
    "ch.qos.logback" % "logback-classic" % "1.1.3"
  )
}